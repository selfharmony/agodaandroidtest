package news.agoda.com.sample.data.repository

import io.reactivex.Flowable
import news.agoda.com.sample.data.api.model.NewsResponse

interface Repository{
    fun getNews(): Flowable<NewsResponse>
}