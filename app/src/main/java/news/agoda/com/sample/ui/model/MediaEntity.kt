package news.agoda.com.sample.ui.model

import org.json.JSONException
import org.json.JSONObject

/**
 * This class represents a media item
 */
data class MediaEntity(
        var url: String,
        var format: String,
        var height: Int,
        var width: Int,
        var type: String,
        var subType: String,
        var caption: String,
        var copyright: String

)