package news.agoda.com.sample.data.api

import io.reactivex.Observable
import news.agoda.com.sample.data.api.model.NewsResponse
import retrofit2.http.GET


interface Api {

    @GET("/bins/nl6jh")
    fun getNews(): Observable<NewsResponse>

}