package news.agoda.com.sample.data.api

import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import news.agoda.com.sample.data.api.model.NewsResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import news.agoda.com.sample.data.api.deserialize.NewsItemDeserializer
import news.agoda.com.sample.data.api.model.NewsItem
import retrofit2.Converter


class ProductionNetworkClient(baseUrl: String, level: HttpLoggingInterceptor.Level): NetworkClient {

    private var api: Api

    init {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = level
        val client = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(createGsonConverter())
                .client(client)
                .build()
        api = retrofit.create(Api::class.java)
    }

    private fun createGsonConverter(): Converter.Factory {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.registerTypeAdapter(NewsItem::class.java, NewsItemDeserializer())
        val gson = gsonBuilder.create()
        return GsonConverterFactory.create(gson)
    }
    override fun getNews(): Flowable<NewsResponse> {
        return api.getNews().toFlowable(BackpressureStrategy.LATEST)
    }
}