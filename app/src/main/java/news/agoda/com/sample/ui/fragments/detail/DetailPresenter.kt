package news.agoda.com.sample.ui.fragments.detail

import news.agoda.com.sample.ui.base.BasePresenter
import news.agoda.com.sample.ui.model.Details

class DetailPresenter () : BasePresenter<Contract.view>(), Contract.presenter {
    var details: Details? = null

    override fun onStart() {}
    override fun onStop() {}

    companion object {
        var storedPresenter: DetailPresenter? = null

        fun savePresenter(presenter: DetailPresenter?){
            storedPresenter = presenter
        }
        fun getSavedPresenter(): DetailPresenter? {
            return storedPresenter
        }
    }
}