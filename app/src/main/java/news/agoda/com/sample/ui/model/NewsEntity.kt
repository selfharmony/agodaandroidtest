package news.agoda.com.sample.ui.model

import android.util.Log

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.util.ArrayList

/**
 * This represents a news item
 */
data class NewsEntity(
        var title: String,
        var summary: String,
        var articleUrl: String,
        var byline: String,
        var publishedDate: String,
        var mediaEntityList: MutableList<MediaEntity>
) {

    fun getMediaUrl(): String? {
        return mediaEntityList.getOrNull(0)?.url
    }

    fun getLargeMediaUrl(): String? {
        return mediaEntityList.getOrNull(3)?.url
    }

    companion object {
        private val TAG = NewsEntity::class.java.simpleName
    }
}
