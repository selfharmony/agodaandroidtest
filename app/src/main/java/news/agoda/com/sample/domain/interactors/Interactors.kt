package news.agoda.com.sample.domain.interactors

import io.reactivex.Flowable
import io.reactivex.Scheduler
import news.agoda.com.sample.data.repository.Repository
import news.agoda.com.sample.domain.mapper.NewsMapper
import news.agoda.com.sample.ui.model.NewsEntity
import java.util.concurrent.Executor

open class GetNewsInteractor(threadScheduler: Executor, uiScheduler: Scheduler, repo: Repository)
    : BaseInteractor<Void, List<NewsEntity>>(threadScheduler, uiScheduler, repo) {
    override fun buildObservable(): Flowable<List<NewsEntity>> {
        return repo.getNews()
                .map { NewsMapper.fromNetwork(it) }
    }
}