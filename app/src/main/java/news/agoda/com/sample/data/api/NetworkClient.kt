package news.agoda.com.sample.data.api

import io.reactivex.Flowable
import news.agoda.com.sample.data.api.model.NewsResponse


interface NetworkClient {

    fun getNews(): Flowable<NewsResponse>
}