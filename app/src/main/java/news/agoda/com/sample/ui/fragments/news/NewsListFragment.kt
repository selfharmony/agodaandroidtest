package news.agoda.com.sample.ui.fragments.news

import android.os.Bundle
import android.service.autofill.FieldClassification
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.Provides
import io.reactivex.Scheduler
import kotlinx.android.synthetic.main.fragment_news_list.*
import news.agoda.com.sample.core.App
import news.agoda.com.sample.data.repository.Repository
import news.agoda.com.sample.databinding.FragmentNewsListBinding
import news.agoda.com.sample.di.PerFragment
import news.agoda.com.sample.domain.interactors.GetNewsInteractor
import news.agoda.com.sample.domain.interactors.Interactor
import news.agoda.com.sample.ui.Router
import news.agoda.com.sample.ui.base.BaseFragment
import news.agoda.com.sample.ui.model.NewsEntity
import java.util.concurrent.Executor
import javax.inject.Inject
import news.agoda.com.sample.ui.base.BaseContract.Presenter



class NewsListFragment : BaseFragment(), Contract.view {
    @Inject
    lateinit var presenter: NewsListPresenter
    lateinit var component: Component
    private val newsAdapter by lazy {
        NewsListAdapter(object : NewsListAdapter.OnItemClickListener {
            override fun onClick(pos: Int, item: NewsEntity) {
                presenter.onItemClicked(pos, item)
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        presenter.view = this
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentNewsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
        presenter.getNews()
    }

    override fun onDestroy() {
        NewsListPresenter.savePresenter(presenter)
        super.onDestroy()
    }

    private fun setView() {
        list.adapter = newsAdapter
        list.layoutManager = LinearLayoutManager(context)
    }

    //region Dagger init
    fun inject() {
        val appComponent = (activity?.application as App).appComponent
        component = DaggerNewsListFragment_Component.builder().module(Module()).component(appComponent).build()
        component.inject(this)
    }

    @PerFragment
    @dagger.Component(
            modules = [(Module::class)],
            dependencies = [(App.Component::class)]
    )
    interface Component : App.Component {
        fun inject(fragment: NewsListFragment)
        fun presenter(): NewsListPresenter
    }

    @dagger.Module
    class Module {
        @Provides
        @PerFragment
        fun provideNewsFragmentPresenter(
                getNewsInteractor: Interactor<Void, List<NewsEntity>>
        ): NewsListPresenter {
            return NewsListPresenter.getSavedPresenter() ?: NewsListPresenter(getNewsInteractor)
        }

        @Provides
        @PerFragment
        fun provideGetNewsInteractor(executor: Executor, scheduler: Scheduler, repo: Repository): Interactor<Void, List<NewsEntity>> {
            return GetNewsInteractor(executor, scheduler, repo)
        }
    }

    //endregion

    override fun showNews(news: List<NewsEntity>) {
        newsAdapter.replaceItems(news)
    }

    override fun navigateToDetail(bundle: Bundle) {
        (activity as Router).navigateToDetail(bundle);
    }

    companion object {
        val TAG = "NewsListFragment"

        fun newInstance(): NewsListFragment {
            val args = Bundle()
            val fragment = NewsListFragment()
            fragment.arguments = args
            return fragment
        }

    }


}