package news.agoda.com.sample.core

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import io.reactivex.Scheduler
import news.agoda.com.sample.data.repository.Repository
import news.agoda.com.sample.di.MainModule
import java.util.concurrent.Executor
import javax.inject.Singleton



class App : Application(){
    var appComponent: Component? = null

    override fun onCreate() {
        super.onCreate()
        appComponent = createAppComponent()
        Fresco.initialize(this);
    }


    private fun createAppComponent(): Component {
        return DaggerApp_Component.builder().mainModule((MainModule(this))).build()
    }

    @Singleton
    @dagger.Component(modules = arrayOf(MainModule::class))
    interface Component {
        fun repo(): Repository
        fun workerThread(): Executor
        fun uiThread(): Scheduler
    }
}