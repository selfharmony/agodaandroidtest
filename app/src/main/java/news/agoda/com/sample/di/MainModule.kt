package news.agoda.com.sample.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import news.agoda.com.sample.BuildConfig
import news.agoda.com.sample.core.API_ENDPOINT
import news.agoda.com.sample.core.APP_PREFERENCES
import news.agoda.com.sample.data.api.NetworkClient
import news.agoda.com.sample.data.api.ProductionNetworkClient
import news.agoda.com.sample.data.repository.ProductionRepository
import news.agoda.com.sample.data.repository.Repository
import news.agoda.com.sample.data.storage.LocalStorage
import news.agoda.com.sample.data.storage.ProductionLocalStorage
import news.agoda.com.sample.domain.interactors.ThreadScheduler
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.Executor
import javax.inject.Named
import javax.inject.Singleton

@dagger.Module
class MainModule(val app: Application) {

    @Singleton
    @Provides
    fun provideWorkerThread(): Executor {
        return ThreadScheduler()
    }

    @Singleton
    @Provides
    fun provideUiThread(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Singleton
    @Provides
    fun provideRepository(networkClient: NetworkClient, localStorage: LocalStorage): Repository {
        return ProductionRepository(networkClient, localStorage)
    }


    @Singleton
    @Provides
    fun provideNetworkClient(@Named("baseUrl") baseUrl: String, level: HttpLoggingInterceptor.Level): NetworkClient {
        return ProductionNetworkClient(baseUrl, level)
    }

    @Singleton
    @Provides
    fun provideLocalStorage(prefs: SharedPreferences): LocalStorage {
        return ProductionLocalStorage(prefs)
    }

    @Singleton
    @Provides
    fun provideLevel(): HttpLoggingInterceptor.Level {
        return if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    }

    @Provides
    @Singleton
    fun providesSharedPreferences(): SharedPreferences {
        return app.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
    }


    @Singleton
    @Provides
    @Named("baseUrl")
    fun provideBaseUrl(): String {
        return API_ENDPOINT
    }

}