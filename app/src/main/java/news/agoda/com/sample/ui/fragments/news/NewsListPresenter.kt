package news.agoda.com.sample.ui.fragments.news

import android.os.Bundle
import io.reactivex.subscribers.DisposableSubscriber
import news.agoda.com.sample.domain.interactors.Interactor
import news.agoda.com.sample.ui.base.BasePresenter
import news.agoda.com.sample.ui.model.NewsEntity


class NewsListPresenter(private val getNewsInteractor: Interactor<Void, List<NewsEntity>>) : BasePresenter<Contract.view>(), Contract.presenter {
    var news: List<NewsEntity>? = null
    override fun onStart() {}
    override fun onStop() {}

    override fun getNews() {
        news?.let {
            view?.showNews(it)
        } ?:
        downloadNews()
    }

    private fun downloadNews() {
        view?.showLoading()
        getNewsInteractor.execute(object : DisposableSubscriber<List<NewsEntity>>(){
            override fun onComplete() {}

            override fun onNext(n: List<NewsEntity>) {
                view?.hideLoading()
                news = n
                view?.showNews(n)
            }

            override fun onError(t: Throwable?) {
                view?.hideLoading()
                t?.printStackTrace()
            }
        })
    }

    override fun onItemClicked(pos: Int, item: NewsEntity) {
        val arguments = Bundle()
        val storyURL = arguments.putString("storyURL", item.articleUrl)
        val title = arguments.putString("title", item.title)
        val summary = arguments.putString("summary", item.summary)
        val imageURL = arguments.putString("imageURL", item.getLargeMediaUrl())
        view?.navigateToDetail(arguments)
    }

    companion object {
        var storedPresenter: NewsListPresenter? = null

        fun savePresenter(presenter: NewsListPresenter?){
            storedPresenter = presenter
        }
        fun getSavedPresenter(): NewsListPresenter? {
            return storedPresenter
        }
    }
}