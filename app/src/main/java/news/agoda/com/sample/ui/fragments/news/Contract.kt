package news.agoda.com.sample.ui.fragments.news

import android.os.Bundle
import news.agoda.com.sample.ui.base.BaseContract
import news.agoda.com.sample.ui.model.NewsEntity

interface Contract {
    interface view : BaseContract.View {
        fun showNews(news: List<NewsEntity>)
        fun navigateToDetail(bundle: Bundle)

    }
    interface presenter : BaseContract.Presenter{
        fun onItemClicked(pos: Int, item: NewsEntity)
        fun getNews()

    }
}