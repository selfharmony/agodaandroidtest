package news.agoda.com.sample.utils

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso

fun View.hide() {
    visibility = View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

fun ImageView.loadImage(image: String) {
    Picasso.with(context)
            .load(image)
            .into(this)
}

fun ImageView.loadImage(resourceId: Int) {
    Picasso.with(context)
            .load(resourceId)
            .into(this)
}
