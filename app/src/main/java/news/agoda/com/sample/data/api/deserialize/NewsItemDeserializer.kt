package news.agoda.com.sample.data.api.deserialize

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import news.agoda.com.sample.data.api.model.MediaItem
import news.agoda.com.sample.data.api.model.NewsItem
import java.lang.reflect.Type


public class NewsItemDeserializer : JsonDeserializer<NewsItem> {
    var gson = Gson()

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): NewsItem {
        val jsonObject = json.asJsonObject

        // Read simple String values.
        val subsection: String? = readToSimpleString(jsonObject, "subsection")
        val itemType: String? = readToSimpleString(jsonObject, "item_type")
        val section: String? = readToSimpleString(jsonObject, "section")
        val jsonMemberAbstract: String? = readToSimpleString(jsonObject, "abstract")
        val title: String? = readToSimpleString(jsonObject, "title")
        val url: String? = readToSimpleString(jsonObject, "url")
        val updatedDate: String? = readToSimpleString(jsonObject, "updated_date")
        val createdDate: String? = readToSimpleString(jsonObject, "created_date")
        val byline: String? = readToSimpleString(jsonObject, "byline")
        val publishedDate: String? = readToSimpleString(jsonObject, "published_date")
        val kicker: String? = readToSimpleString(jsonObject, "kicker")
        val materialTypeFacet: String? = readToSimpleString(jsonObject, "material_type_facet")

        // Read the dynamic parameters object.
        val geoFacet: List<String?>? = readToStringsList(jsonObject, "geo_facet")
        val orgFacet: List<String?>? = readToStringsList(jsonObject, "org_facet")
        val perFacet: List<String?>? = readToStringsList(jsonObject, "per_facet")
        val desFacet: List<String?>? = readToStringsList(jsonObject, "des_facet")
        val multimedia: List<MediaItem?>? = readMultimediaList(jsonObject, "multimedia")


        val result = NewsItem()
        result.subsection = subsection
        result.itemType = itemType
        result.section = section
        result.jsonMemberAbstract = jsonMemberAbstract
        result.title = title
        result.url = url
        result.updatedDate = updatedDate
        result.createdDate = createdDate
        result.byline = byline
        result.publishedDate = publishedDate
        result.kicker = kicker
        result.materialTypeFacet = materialTypeFacet

        result.multimedia = multimedia
        result.geoFacet = geoFacet
        result.orgFacet = orgFacet
        result.perFacet = perFacet
        result.desFacet = desFacet

        return result
    }

    fun readToSimpleString(jsonObject: JsonObject, key: String) =
            jsonObject.get(key).asString

    fun readToStringsList(jsonObject: JsonObject, key: String): List<String?>? {
        val element = jsonObject.get(key) ?: return null
        val result = ArrayList<String>()
         if (element.isJsonArray) {
            val parsedMedia = gson.fromJson<List<String>>(element, genericType<List<String>>())
            result.addAll(parsedMedia)
        } else if (element.isJsonPrimitive) {
            val parsedMedia = gson.fromJson<String>(element, String::class.java)
            result.add(parsedMedia)
        }
        return result
    }


    fun readMultimediaList(jsonObject: JsonObject, key: String): List<MediaItem?>? {
        val result = ArrayList<MediaItem?>()
        val element = jsonObject.get(key) ?: return null

        if (element.isJsonObject) {
            val parsedMedia = gson.fromJson<MediaItem>(element, genericType<MediaItem>())
            result.add(parsedMedia)
        } else if (element.isJsonArray) {
            val parsedMedia = gson.fromJson<List<MediaItem>>(element, genericType<List<MediaItem>>())
            result.addAll(parsedMedia)
        }
        return result
    }
    inline fun <reified T> genericType() = object: TypeToken<T>() {}.type
}