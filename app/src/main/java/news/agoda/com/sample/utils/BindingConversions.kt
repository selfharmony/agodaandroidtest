package news.agoda.com.sample.utils

import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.view.DraweeView
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.request.ImageRequest
import com.squareup.picasso.Picasso

object BindingConversions {
    @JvmStatic
    @BindingAdapter("android:frescoSrc")
    fun loadImage(imageView: SimpleDraweeView, url: String?) {
        url?.let {
            if (!it.isEmpty()) {
                imageView.controller = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(
                                ImageRequest.fromUri(Uri.parse(url))
                        )
                        .setOldController(imageView.controller)
                        .build()
            }
        }
    }
}