package news.agoda.com.sample.data.api.model

import com.google.gson.annotations.SerializedName

data class MediaItem(

        @field:SerializedName("copyright")
        var copyright: String? = null,

        @field:SerializedName("subtype")
        var subtype: String? = null,

        @field:SerializedName("format")
        var format: String? = null,

        @field:SerializedName("width")
        var width: Int? = null,

        @field:SerializedName("caption")
        var caption: String? = null,

        @field:SerializedName("type")
        var type: String? = null,

        @field:SerializedName("url")
        var url: String? = null,

        @field:SerializedName("height")
        var height: Int? = null
)