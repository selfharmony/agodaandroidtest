package news.agoda.com.sample.ui.fragments.detail

import news.agoda.com.sample.ui.base.BaseContract

interface Contract {
    interface view : BaseContract.View {

    }

    interface presenter : BaseContract.Presenter {

    }
}