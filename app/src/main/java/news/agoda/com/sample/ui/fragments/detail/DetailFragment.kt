package news.agoda.com.sample.ui.fragments.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.Provides
import kotlinx.android.synthetic.main.fragment_detail.*
import news.agoda.com.sample.core.App
import news.agoda.com.sample.databinding.FragmentDetailBinding
import news.agoda.com.sample.di.PerFragment
import news.agoda.com.sample.ui.Router
import news.agoda.com.sample.ui.base.BaseFragment
import news.agoda.com.sample.ui.model.Details
import javax.inject.Inject

class DetailFragment: BaseFragment(), Contract.view {

    @Inject
    lateinit var presenter: DetailPresenter
    lateinit var component: Component
    val details: Details by lazy {
        presenter.details ?:getStoredDetails()
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        inject()
        presenter.view = this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentDetailBinding.inflate(inflater, container, false)
        binding.model = details
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
    }

    //region Dagger init
    fun inject() {
        val appComponent = (activity?.application as App).appComponent
        component = DaggerDetailFragment_Component.builder().module(Module()).component(appComponent).build()
        component.inject(this)
    }

    @PerFragment
    @dagger.Component(
            modules = [(Module::class)],
            dependencies = [(App.Component::class)]
    )
    interface Component : App.Component {
        fun inject(fragment: DetailFragment)
        fun presenter(): DetailPresenter
    }

    @dagger.Module
    class Module {
        @Provides
        @PerFragment
        fun provideDetailFragmentPresenter(): DetailPresenter {
            return DetailPresenter.getSavedPresenter() ?: DetailPresenter()
        }
    }
    //endregion

    private fun getStoredDetails(): Details {
        val storyURL = arguments?.getString("storyURL")
        val title = arguments?.getString("title")
        val summary = arguments?.getString("summary")
        val imageURL = arguments?.getString("imageURL")
        val details = Details(
                title = title,
                summary = summary,
                storyURL = storyURL,
                imageURL = imageURL)
        presenter.details = details
        return details

    }

    private fun setView() {
        full_story_link.setOnClickListener{
            details.storyURL?.let {
                (activity as Router).openUrl(it)
            }
        }
    }

    companion object {
        val TAG = "DetailFragment"

        fun newInstance(args : Bundle): DetailFragment {
            val fragment = DetailFragment()
            fragment.arguments = args
            return fragment
        }
    }
}