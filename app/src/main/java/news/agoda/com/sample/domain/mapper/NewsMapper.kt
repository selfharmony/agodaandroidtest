package news.agoda.com.sample.domain.mapper

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import news.agoda.com.sample.data.api.model.MediaItem
import news.agoda.com.sample.data.api.model.NewsResponse
import news.agoda.com.sample.ui.model.MediaEntity
import news.agoda.com.sample.ui.model.NewsEntity
import java.util.*
import kotlin.collections.ArrayList


object NewsMapper {

    fun fromNetwork(source: NewsResponse?): List<NewsEntity> {
        source?.let {
            val mappedNews = ArrayList<NewsEntity>()

            it.results?.mapNotNullTo(mappedNews) {
                NewsEntity(
                        title = it?.title ?: "No Title",
                        summary = it?.jsonMemberAbstract ?: "",
                        articleUrl = it?.url ?: "",
                        byline = it?.byline ?: "",
                        publishedDate = it?.publishedDate ?: "",
                        mediaEntityList = mapMultimedia(it?.multimedia)
                )
            }
            return mappedNews
        } ?: throw Exception("NewsResponse is null")
    }

    private fun mapMultimedia(multimedia: List<MediaItem?>?): MutableList<MediaEntity> {
        val mappedMultimedia = ArrayList<MediaEntity>()
        multimedia?.mapNotNullTo(mappedMultimedia) {
            MediaEntity(
                    url = it?.url ?: "",
                    format = it?.format ?: "",
                    height = it?.height ?: 0,
                    width = it?.width ?: 0,
                    type = it?.type ?: "",
                    subType = it?.subtype ?: "",
                    caption = it?.caption ?: "",
                    copyright = it?.copyright ?: ""
            )
        }
        return mappedMultimedia
    }
}