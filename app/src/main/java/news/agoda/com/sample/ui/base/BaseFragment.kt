package news.agoda.com.sample.ui.base

import android.support.v4.app.Fragment
import news.agoda.com.sample.ui.FragmentsUiManager


abstract class BaseFragment : Fragment(), BaseContract.View {

    var TAG = tag
    override fun showToast(message: String) {
        (activity as? FragmentsUiManager)?.showToast(message)
    }

    override fun showLoading() {
        (activity as? FragmentsUiManager)?.showLoading()
    }

    override fun hideLoading() {
        (activity as? FragmentsUiManager)?.hideLoading()
    }

    open fun isFullScreen(): Boolean = false

    open fun onBackPressedCallback(): Boolean = true
}
