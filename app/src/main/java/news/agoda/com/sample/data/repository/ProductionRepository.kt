package news.agoda.com.sample.data.repository

import io.reactivex.Flowable
import news.agoda.com.sample.data.api.NetworkClient
import news.agoda.com.sample.data.api.model.NewsResponse
import news.agoda.com.sample.data.storage.LocalStorage

class ProductionRepository(val networkClient: NetworkClient,
                           val localStorage: LocalStorage) : Repository {

    override fun getNews() = networkClient.getNews()
}