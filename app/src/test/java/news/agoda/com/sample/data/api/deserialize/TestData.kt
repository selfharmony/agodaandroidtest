package news.agoda.com.sample.data.api.deserialize

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import news.agoda.com.sample.data.api.model.MediaItem

object TestData {

    val gson = Gson()

    val TEST_KEY = "test-key"
    val TEST_STRING = "string-result"
    val TEST_STRING_LIST = listOf<String>("string-result")
    val TEST_MULTIMEDIA: MediaItem by lazy {
        val item = MediaItem()
        item.url = TEST_STRING
        item.caption = TEST_STRING
        item.copyright = TEST_STRING
        item
    }
    val TEST_MULTIMEDIA_LIST = listOf<MediaItem>(TEST_MULTIMEDIA)

    val TEST_JSON_OBJECT_WITH_STRING: JsonObject by lazy {
        val rootObject = JsonObject()
        rootObject.addProperty(TEST_KEY, TEST_STRING)
        rootObject
    }

    val TEST_JSON_OBJECT_WITH_STRING_LIST: JsonObject by lazy {
        val rootObject = JsonObject()
        val childArray = JsonArray()
        childArray.add(TEST_STRING)
        rootObject.add(TEST_KEY, childArray)
        rootObject
    }

    val TEST_JSON_OBJECT_WITH_MULTIMEDIA_LIST: JsonObject by lazy {
        val rootObject = JsonObject()
        rootObject.add(TEST_KEY, gson.toJsonTree(TEST_MULTIMEDIA_LIST))
        rootObject
    }

    val TEST_JSON_OBJECT_WITH_MULTIMEDIA: JsonObject by lazy {
        val rootObject = JsonObject()
        rootObject.add(TEST_KEY, gson.toJsonTree(TEST_MULTIMEDIA))
        rootObject
    }


}
